# Projet de la société weakspot

## le lien vers le site firebase:

hosting URL: https://weakspot-pme-2.web.app/

Project Console: https://console.firebase.google.com/project/weakspot-pme-2/overview

## les couleurs principales:

* couleur bg du main: #1487cb

* couleur bg du footer: #272727

* couleur du bg des cards: rgba(0, 0, 0, 0.25)

    * Avec des border-radius de: border-radius: 10px;

    * Des box-shadow (sur le hover) de: box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);

* couleur bg des buttons: #002f7d

* couleur bg des sections details: #164c92

* couleur bg des header cards dans details: #86cbf5

* couleur bg des main cards dans details: #123d75

* couleur principales des textes: #fff (blanc)

* couleur des textes sur bg du main: #bbe5ff

* couleur des textes sur bg des sections dans details: #86cbf5 ou #fff (blanc)

* couleur des textes sur fond blanc: #212529


## les images:

../assets/img/####.svg

## les fonts:

Segoe UI, Roboto, Helvetica Neue , Arial sans-serif (laquelle vraiment?)
